﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)]!!!*Q(C=\:3^==-Q$%;`Z&amp;+Y63&lt;)?165[&lt;W#6]!+&lt;KVTYUO:$CNI";W!&amp;&lt;S#6V#?;$D`C:PE,E6)1Z9_A-!436&amp;K&lt;3(&gt;;,L5&amp;N_W]M^`,&lt;9^4G5P`L-W^QJ_[[]BU[P_9@T:0ZXM@@W8MJ`6`Z,`5N/X\&gt;``\``&lt;`MP&lt;?XPZ)`D&gt;*FUV5CSRQ"ST^H:4IC&gt;[IC&gt;[IC&gt;[I!&gt;[I!&gt;[I!?[ITO[ITO[ITO[I2O[I2O[I2N[[_B#&amp;\L1G:7E?&amp;)I3:IE3)*"58**?"+?B#@BY6%*4]+4]#1]#1]B3HA3HI1HY5FY'+;%*_&amp;*?"+?B)&gt;5D32&lt;2Y=HY3'^!J[!*_!*?!)?3CLA#1##9E(C)!E9#JT"4=!4]!1]X#LA#8A#HI!HY-'NA#@A#8A#HI#()7V7IN(-(2U?UMDB=8A=(I@(Y3'V("[(R_&amp;R?"Q?SMHB=8A=#+?AERQ%/9/=!/@"Y8&amp;Y_*0$Y`!Y0![0QY/LL:#XG:FJZIY/D]&amp;D]"A]"I`"1QI:0!;0Q70Q'$SEF=&amp;D]"A]"I`"1SE:0!;0Q7/!'%5J,S/:-&gt;!)-A3$BV`&lt;,&gt;:7+2K*N6[@ZGGDKD;A;G/J.IRK)[A77,6QKA624&lt;2K!F54IXJBV9OI!&amp;7&amp;61F6A4JS07!D.G"\&lt;)/NM27WR,JZ[!]((I^((1Y(D?/I92CUX__VW7SU8K_V7KWU8#\6&gt;&gt;XT;86(0\7L]\GUZ8ZXP_CX$^(XNU/`@2T\8==T_NH`'X9[F`\#W;BL4;_/??&lt;I#6Z@4!5!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">49 54 48 48 56 48 49 57 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 12 252 1 100 1 100 80 84 72 48 0 0 0 22 0 1 0 2 7 77 111 100 117 108 101 115 9 51 55 48 52 52 46 112 110 103 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 0 0 0 0 0 0 0 0 0 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 0 0 0 255 213 136 0 0 0 255 213 136 0 0 0 0 0 0 0 0 0 255 213 136 0 0 0 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 255 213 136 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="Public API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Arguments" Type="Folder">
			<Item Name="Request" Type="Folder">
				<Item Name="Stop Argument--cluster.ctl" Type="VI" URL="../Stop Argument--cluster.ctl"/>
				<Item Name="Show Panel Argument--cluster.ctl" Type="VI" URL="../Show Panel Argument--cluster.ctl"/>
				<Item Name="Hide Panel Argument--cluster.ctl" Type="VI" URL="../Hide Panel Argument--cluster.ctl"/>
				<Item Name="Show Diagram Argument--cluster.ctl" Type="VI" URL="../Show Diagram Argument--cluster.ctl"/>
				<Item Name="Get Module Execution Status Argument--cluster.ctl" Type="VI" URL="../Get Module Execution Status Argument--cluster.ctl"/>
				<Item Name="Set Sub Panel Argument--cluster.ctl" Type="VI" URL="../Set Sub Panel Argument--cluster.ctl"/>
				<Item Name="Slice Level Adjust-Enum.ctl" Type="VI" URL="../Slice Level Adjust-Enum.ctl"/>
				<Item Name="Update Device Object Argument--cluster.ctl" Type="VI" URL="../Update Device Object Argument--cluster.ctl"/>
				<Item Name="Slice Level Enable Argument--cluster.ctl" Type="VI" URL="../Slice Level Enable Argument--cluster.ctl"/>
				<Item Name="Slice Level Adjust Argument--cluster.ctl" Type="VI" URL="../Slice Level Adjust Argument--cluster.ctl"/>
				<Item Name="Select Device Page Argument--cluster.ctl" Type="VI" URL="../Select Device Page Argument--cluster.ctl"/>
				<Item Name="Wait For Did Read Write Argument--cluster.ctl" Type="VI" URL="../Wait For Did Read Write Argument--cluster.ctl"/>
				<Item Name="Wait For Did Read Write (Reply Payload)--cluster.ctl" Type="VI" URL="../Wait For Did Read Write (Reply Payload)--cluster.ctl"/>
				<Item Name="Set De-Emphasis Argument--cluster.ctl" Type="VI" URL="../Set De-Emphasis Argument--cluster.ctl"/>
				<Item Name="Set CDR Bypass Argument--cluster.ctl" Type="VI" URL="../Set CDR Bypass Argument--cluster.ctl"/>
				<Item Name="Set Power Down Argument--cluster.ctl" Type="VI" URL="../Set Power Down Argument--cluster.ctl"/>
				<Item Name="Get CDR Bypass Status Argument--cluster.ctl" Type="VI" URL="../Get CDR Bypass Status Argument--cluster.ctl"/>
				<Item Name="Get CDR Bypass Status (Reply Payload)--cluster.ctl" Type="VI" URL="../Get CDR Bypass Status (Reply Payload)--cluster.ctl"/>
				<Item Name="Get Power Down Status Argument--cluster.ctl" Type="VI" URL="../Get Power Down Status Argument--cluster.ctl"/>
				<Item Name="Get Power Down Status (Reply Payload)--cluster.ctl" Type="VI" URL="../Get Power Down Status (Reply Payload)--cluster.ctl"/>
				<Item Name="Get De-Emphasis Argument--cluster.ctl" Type="VI" URL="../Get De-Emphasis Argument--cluster.ctl"/>
				<Item Name="Get De-Emphasis (Reply Payload)--cluster.ctl" Type="VI" URL="../Get De-Emphasis (Reply Payload)--cluster.ctl"/>
				<Item Name="Status-Cluster.ctl" Type="VI" URL="../Status-Cluster.ctl"/>
				<Item Name="Value-Cluster.ctl" Type="VI" URL="../Value-Cluster.ctl"/>
				<Item Name="Regerist CTRL Ref Argument--cluster.ctl" Type="VI" URL="../Regerist CTRL Ref Argument--cluster.ctl"/>
			</Item>
			<Item Name="Broadcast" Type="Folder">
				<Item Name="Did Init Argument--cluster.ctl" Type="VI" URL="../Did Init Argument--cluster.ctl"/>
				<Item Name="Status Updated Argument--cluster.ctl" Type="VI" URL="../Status Updated Argument--cluster.ctl"/>
				<Item Name="Error Reported Argument--cluster.ctl" Type="VI" URL="../Error Reported Argument--cluster.ctl"/>
			</Item>
		</Item>
		<Item Name="Requests" Type="Folder">
			<Item Name="Show Panel.vi" Type="VI" URL="../Show Panel.vi"/>
			<Item Name="Hide Panel.vi" Type="VI" URL="../Hide Panel.vi"/>
			<Item Name="Stop Module.vi" Type="VI" URL="../Stop Module.vi"/>
			<Item Name="Show Diagram.vi" Type="VI" URL="../Show Diagram.vi"/>
			<Item Name="Set Sub Panel.vi" Type="VI" URL="../Set Sub Panel.vi"/>
			<Item Name="Update Device Object.vi" Type="VI" URL="../Update Device Object.vi"/>
			<Item Name="Select Device Page.vi" Type="VI" URL="../Select Device Page.vi"/>
			<Item Name="Wait For Did Read Write.vi" Type="VI" URL="../Wait For Did Read Write.vi"/>
			<Item Name="Slice Level Enable.vi" Type="VI" URL="../Slice Level Enable.vi"/>
			<Item Name="Slice Level Adjust.vi" Type="VI" URL="../Slice Level Adjust.vi"/>
			<Item Name="Set De-Emphasis.vi" Type="VI" URL="../Set De-Emphasis.vi"/>
			<Item Name="Set CDR Bypass.vi" Type="VI" URL="../Set CDR Bypass.vi"/>
			<Item Name="Set Power Down.vi" Type="VI" URL="../Set Power Down.vi"/>
			<Item Name="Get CDR Bypass Status.vi" Type="VI" URL="../Get CDR Bypass Status.vi"/>
			<Item Name="Get Power Down Status.vi" Type="VI" URL="../Get Power Down Status.vi"/>
			<Item Name="Get De-Emphasis.vi" Type="VI" URL="../Get De-Emphasis.vi"/>
			<Item Name="Regerist CTRL Ref.vi" Type="VI" URL="../Regerist CTRL Ref.vi"/>
		</Item>
		<Item Name="Start Module.vi" Type="VI" URL="../Start Module.vi"/>
		<Item Name="Synchronize Module Events.vi" Type="VI" URL="../Synchronize Module Events.vi"/>
		<Item Name="Obtain Broadcast Events for Registration.vi" Type="VI" URL="../Obtain Broadcast Events for Registration.vi"/>
		<Item Name="Dynamic Start Module.vi" Type="VI" URL="../Dynamic Start Module.vi"/>
		<Item Name="Check Status.vi" Type="VI" URL="../Check Status.vi"/>
	</Item>
	<Item Name="Broadcasts" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Broadcast Events--cluster.ctl" Type="VI" URL="../Broadcast Events--cluster.ctl"/>
		<Item Name="Obtain Broadcast Events.vi" Type="VI" URL="../Obtain Broadcast Events.vi"/>
		<Item Name="Destroy Broadcast Events.vi" Type="VI" URL="../Destroy Broadcast Events.vi"/>
		<Item Name="Module Did Init.vi" Type="VI" URL="../Module Did Init.vi"/>
		<Item Name="Status Updated.vi" Type="VI" URL="../Status Updated.vi"/>
		<Item Name="Error Reported.vi" Type="VI" URL="../Error Reported.vi"/>
		<Item Name="Module Did Stop.vi" Type="VI" URL="../Module Did Stop.vi"/>
		<Item Name="Update Module Execution Status.vi" Type="VI" URL="../Update Module Execution Status.vi"/>
	</Item>
	<Item Name="Requests" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Request Events--cluster.ctl" Type="VI" URL="../Request Events--cluster.ctl"/>
		<Item Name="Obtain Request Events.vi" Type="VI" URL="../Obtain Request Events.vi"/>
		<Item Name="Destroy Request Events.vi" Type="VI" URL="../Destroy Request Events.vi"/>
		<Item Name="Get Module Execution Status.vi" Type="VI" URL="../Get Module Execution Status.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="Table" Type="Folder">
				<Item Name="Trim Array.vi" Type="VI" URL="../Trim Array.vi"/>
				<Item Name="Initialize Table.vi" Type="VI" URL="../Initialize Table.vi"/>
				<Item Name="Update Table.vi" Type="VI" URL="../Update Table.vi"/>
			</Item>
			<Item Name="Get CTRL Value" Type="Folder">
				<Item Name="Page" Type="Folder">
					<Item Name="Get  CTRL Data.vi" Type="VI" URL="../Get  CTRL Data.vi"/>
					<Item Name="Get Contol Info Value.vi" Type="VI" URL="../Get Contol Info Value.vi"/>
					<Item Name="Get RSSI&amp;Power Control Info.vi" Type="VI" URL="../Get RSSI&amp;Power Control Info.vi"/>
					<Item Name="Get Alarm Mask Info.vi" Type="VI" URL="../Get Alarm Mask Info.vi"/>
				</Item>
				<Item Name="Sub Vi" Type="Folder">
					<Item Name="Type Define" Type="Folder">
						<Item Name="Enum Index.ctl" Type="VI" URL="../Enum Index.ctl"/>
						<Item Name="Boolean Index.ctl" Type="VI" URL="../Boolean Index.ctl"/>
						<Item Name="Boolean Cluster Index.ctl" Type="VI" URL="../Boolean Cluster Index.ctl"/>
						<Item Name="Enum Cluster Index.ctl" Type="VI" URL="../Enum Cluster Index.ctl"/>
					</Item>
					<Item Name="Replace Boolean Array Data.vi" Type="VI" URL="../Replace Boolean Array Data.vi"/>
					<Item Name="Replace Enum Array.vi" Type="VI" URL="../Replace Enum Array.vi"/>
					<Item Name="Replace Enum Data.vi" Type="VI" URL="../Replace Enum Data.vi"/>
					<Item Name="Replace Boolean Data.vi" Type="VI" URL="../Replace Boolean Data.vi"/>
				</Item>
			</Item>
			<Item Name="Update CTRL" Type="Folder">
				<Item Name="Page" Type="Folder">
					<Item Name="Update Status Info.vi" Type="VI" URL="../Update Status Info.vi"/>
					<Item Name="Update Contol Info.vi" Type="VI" URL="../Update Contol Info.vi"/>
					<Item Name="Update Alarm Mask Info.vi" Type="VI" URL="../Update Alarm Mask Info.vi"/>
					<Item Name="Update RSSI&amp;Power Control Info.vi" Type="VI" URL="../Update RSSI&amp;Power Control Info.vi"/>
				</Item>
				<Item Name="Update Control Value.vi" Type="VI" URL="../Update Control Value.vi"/>
			</Item>
			<Item Name="Debug" Type="Folder">
				<Item Name="Get Simulate Data.vi" Type="VI" URL="../Get Simulate Data.vi"/>
				<Item Name="Write Simulate Data.vi" Type="VI" URL="../Write Simulate Data.vi"/>
			</Item>
			<Item Name="Replace Password Address.vi" Type="VI" URL="../Replace Password Address.vi"/>
			<Item Name="Insert VI to Sub Panel.vi" Type="VI" URL="../Insert VI to Sub Panel.vi"/>
			<Item Name="Message Queue Logger.vi" Type="VI" URL="../Message Queue Logger.vi"/>
			<Item Name="Set Caller FP Size By Module.vi" Type="VI" URL="../Set Caller FP Size By Module.vi"/>
			<Item Name="Visible Decoration.vi" Type="VI" URL="../Visible Decoration.vi"/>
			<Item Name="Get Ref.vi" Type="VI" URL="../Get Ref.vi"/>
			<Item Name="Short Menu Act" Type="VI" URL="../Clone Registration/Short Menu Act"/>
			<Item Name="Shortcut Menu Selection.vi" Type="VI" URL="../Shortcut Menu Selection.vi"/>
		</Item>
		<Item Name="Control" Type="Folder">
			<Item Name="Module Data--cluster.ctl" Type="VI" URL="../Module Data--cluster.ctl"/>
		</Item>
		<Item Name="Class" Type="Folder">
			<Item Name="Get Queue Info.lvclass" Type="LVClass" URL="../../Get Queue Info/Get Queue Info.lvclass"/>
		</Item>
		<Item Name="Init Module.vi" Type="VI" URL="../Init Module.vi"/>
		<Item Name="Handle Exit.vi" Type="VI" URL="../Handle Exit.vi"/>
		<Item Name="Close Module.vi" Type="VI" URL="../Close Module.vi"/>
		<Item Name="Module Name--constant.vi" Type="VI" URL="../Module Name--constant.vi"/>
		<Item Name="Module Timeout--constant.vi" Type="VI" URL="../Module Timeout--constant.vi"/>
		<Item Name="Module Not Running--error.vi" Type="VI" URL="../Module Not Running--error.vi"/>
		<Item Name="Module Not Synced--error.vi" Type="VI" URL="../Module Not Synced--error.vi"/>
		<Item Name="Module Not Stopped--error.vi" Type="VI" URL="../Module Not Stopped--error.vi"/>
		<Item Name="Module Running as Singleton--error.vi" Type="VI" URL="../Module Running as Singleton--error.vi"/>
		<Item Name="Module Running as Cloneable--error.vi" Type="VI" URL="../Module Running as Cloneable--error.vi"/>
		<Item Name="Initialize Controls Shortcut Menu.vi" Type="VI" URL="../Initialize Controls Shortcut Menu.vi"/>
		<Item Name="Set Shourtcut Menu to Controls.vi" Type="VI" URL="../Set Shourtcut Menu to Controls.vi"/>
	</Item>
	<Item Name="Module Sync" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Destroy Sync Refnums.vi" Type="VI" URL="../Destroy Sync Refnums.vi"/>
		<Item Name="Get Sync Refnums.vi" Type="VI" URL="../Get Sync Refnums.vi"/>
		<Item Name="Synchronize Caller Events.vi" Type="VI" URL="../Synchronize Caller Events.vi"/>
		<Item Name="Wait on Event Sync.vi" Type="VI" URL="../Wait on Event Sync.vi"/>
		<Item Name="Wait on Module Sync.vi" Type="VI" URL="../Wait on Module Sync.vi"/>
		<Item Name="Wait on Stop Sync.vi" Type="VI" URL="../Wait on Stop Sync.vi"/>
	</Item>
	<Item Name="Multiple Instances" Type="Folder">
		<Item Name="Module Ring" Type="Folder">
			<Item Name="Init Select Module Ring.vi" Type="VI" URL="../Init Select Module Ring.vi"/>
			<Item Name="Update Select Module Ring.vi" Type="VI" URL="../Update Select Module Ring.vi"/>
			<Item Name="Addressed to This Module.vi" Type="VI" URL="../Addressed to This Module.vi"/>
		</Item>
		<Item Name="Is Safe to Destroy Refnums.vi" Type="VI" URL="../Is Safe to Destroy Refnums.vi"/>
		<Item Name="Clone Registration.lvlib" Type="Library" URL="../Clone Registration/Clone Registration.lvlib"/>
		<Item Name="Test Clone Registration API.vi" Type="VI" URL="../Clone Registration/Test Clone Registration API.vi"/>
		<Item Name="Get Module Running State.vi" Type="VI" URL="../Get Module Running State.vi"/>
		<Item Name="Module Running State--enum.ctl" Type="VI" URL="../Module Running State--enum.ctl"/>
	</Item>
	<Item Name="Main.vi" Type="VI" URL="../Main.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</Library>
