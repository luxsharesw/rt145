/*
 * MA37044_RX_SRAM.h
 *
 *  Created on: 2016/2/29
 *      Author: Ivan_Lin
 */

#ifndef RT145_MAP_H_
#define RT145_MAP_H_
#include <stdint.h>

#define RT145_RX_AD    0x34
#define RT145_CHIPID   0x00

extern void RT145_RESET();
extern void RT145_Init();
extern void RT145_Write_All();
extern void RT145_Read_All();

extern unsigned int Change_RSSI_OUTPUT(unsigned char RSSI_CH);
extern void RX_Force_Mute(unsigned char Mute_CH);
extern unsigned int RSSI_37044_Internal( unsigned char RSSI_CH );


xdata struct RT145_MEMORY
{
	unsigned char CHIPID ;               			// reg. address 0x00  R   SRAM Address 0x80
	unsigned char LOL_LOS_STATUS  ;      			// reg. address 0x01  R   SRAM Address 0x81
	unsigned char LOL_NLAT_LOL_OR_LOS_STATUS  ;     // reg. address 0x02  R   SRAM Address 0x82
	unsigned char LOS_NLAT_STATUS  ;   				// reg. address 0x03  R   SRAM Address 0x83
	unsigned char ADC_OUT  ;            			// reg. address 0x04  R   SRAM Address 0x84
	unsigned char AGC_RSSI_ADC_OUT ;        	    // reg. address 0x05  R   SRAM Address 0x85

	unsigned char Reserved_1 ;      		        // reg. address 0x06  R   SRAM Address 0x86

	unsigned char SOFT_RESET ;           			// reg. address 0x07  RW  SRAM Address 0x87

	unsigned char GLOBLE_INTERFACE_1 ;          	// reg. address 0x08  RW  SRAM Address 0x88
	unsigned char MCLKEN_UNLOCK_THRS ;              // reg. address 0x09  RW  SRAM Address 0x89
	unsigned char SW_BIST_TP_CDR_LOOP_BW  ;         // reg. address 0x0A  RW  SRAM Address 0x8A
	unsigned char DATA_POL  ;             			// reg. address 0x0B  RW  SRAM Address 0x8B
	unsigned char CDR_BYP_PWD  ;         			// reg. address 0x0C  RW  SRAM Address 0x8C
	unsigned char LOL_LOS_MASK  ;             		// reg. address 0x0D  RW  SRAM Address 0x8D
	unsigned char LOL_LOS_CLEAR  ;            		// reg. address 0x0E  RW  SRAM Address 0x8E
	unsigned char LOL_LOS_MODE  ;        			// reg. address 0x0F  RW  SRAM Address 0x8F
	unsigned char TIA_RATE  ;       				// reg. address 0x10  RW  SRAM Address 0x90
	unsigned char CH01_LOS_THRS  ;       	 		// reg. address 0x11  RW  SRAM Address 0x91
	unsigned char CH23_LOS_THRS  ;       			// reg. address 0x12  RW  SRAM Address 0x92
	unsigned char LOS_HYST  ;            			// reg. address 0x13  RW  SRAM Address 0x93
	unsigned char CH01_LA_SLICE  ;            		// reg. address 0x14  RW  SRAM Address 0x94
	unsigned char CH23_LA_SLICE  ;       			// reg. address 0x15  RW  SRAM Address 0x95
	unsigned char AGC_THRS  ;              			// reg. address 0x16  RW  SRAM Address 0x96
	unsigned char DIS_AUTOMUTE_MUTE_FORCE  ;        // reg. address 0x17  RW  SRAM Address 0x97
	unsigned char CH01_OUT_SWING  ;            		// reg. address 0x18  RW  SRAM Address 0x98
	unsigned char CH23_OUT_SWING  ;            		// reg. address 0x19  RW  SRAM Address 0x99
	unsigned char CH01_DE_EMPH  ;            		// reg. address 0x1A  RW  SRAM Address 0x9A
	unsigned char CH23_DE_EMPH  ;         			// reg. address 0x1B  RW  SRAM Address 0x9B

	unsigned char DDMI_SETTING_1  ;         		// reg. address 0x1C  RW  SRAM Address 0x9C
	unsigned char DDMI_SETTING_2  ; 				// reg. address 0x1D  RW  SRAM Address 0x9D
	unsigned char DDMI_SETTING_3  ;					// reg. address 0x1E  RW  SRAM Address 0x9E
	unsigned char DDMI_SETTING_4  ;         		// reg. address 0x1F  RW  SRAM Address 0x9F

	unsigned char POWER_CONTROL_1  ;         		// reg. address 0x20  RW  SRAM Address 0xA0
	unsigned char POWER_CONTROL_2  ;         		// reg. address 0x21  RW  SRAM Address 0xA1
	unsigned char RING_OSC_SETTING  ;         		// reg. address 0x22  RW  SRAM Address 0xA2

	unsigned char RSVD[85];              			// 0xA3 - 0xF7

	unsigned char Direct_AD;             			// Direct_Control Address RW  SRAM Address 0xF8
	unsigned char Direct_Data[5];        			// Direct_Control Data    RW  SRAM Address 0xF9 - 0xFD
	unsigned char Direct_RW;             			// Direct_Control RW      RW  SRAM Address 0xFE
	unsigned char Direct_EN;             			// Direct_Control RW      RW  SRAM Address 0xFF
};

extern xdata struct RT145_MEMORY_MAP;

#define RT145_SIZE   sizeof(MA37044_MEMORY_MAP)

#endif /* RT145_MAP_H_ */
